#pragma once

class Point {

	float x, y;

public:
	Point();
	void setX(float);
	void setY(float);

	float getX();
	float getY();



};