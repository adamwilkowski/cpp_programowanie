#include <iostream>

using namespace std;

int binarysearch(int *T, int n, int p);

// w3 str.9
//program glowny:
int main()
{
	// - utworzenie tablicy liczb calkowitych (posortowanej)
	int T[10] = { 0, 3, 5, 8, 9, 12, 15, 23, 30, 100 };
	int n = 10;
	// - wczytanie szukanej liczby
	cout << "Program wyszukuje podana liczbe w tablicy: " << endl;
	cout << "Podaj szukana liczbe: ";
	int p;
	cin >> p;

	// - wyswietlnie wyniku wyszukiwania
	int result;
	result = binarysearch(T, n, p);
	if (result == -1)
	{
		cout << " tej liczby nie ma tam" << endl;
	}
	else
	{
	
	cout << " ta liczba to" << result << endl;
	}
	system("pause");
	return 0;
}


// funkcja binarysearch(..):
int binarysearch(int *T, int n, int p)
{
	int bottom;
	int gora = n - 1;
	int result;
	int srodek;
	bottom = 0;
	
	
	while (true)
	{
		if (bottom > gora)
		{
			
			return -1;
		}
		else
		{
			srodek = ((bottom + gora) / 2);

			{

				if (T[srodek] < p)
				{
					bottom = srodek + 1;

				}
				else if (T[srodek] == p)
				{
					result = srodek;
					return result;
				}
				else if (T[srodek] > p)
				{
					gora = srodek - 1;
				}
			}
		}
	}
	
}