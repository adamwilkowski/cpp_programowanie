#include <iostream>

using namespace std;

// - funkcja f(x)
// - jakas prosta funkcja, ktorej miejsca zerwoe znamy

double f(double x);
double bisection(double a, double b, double eps);

int main()
{
	// - wczytaj a, b, eps
	cout << "Program znajduje miejsce zerowe zadanej funkcji w przediale [a, b] z dokladnoscia eps." << endl;
	cout << "Podaj a= ";
	double a, b, eps;
	cin >> a;

	cout << "Podaj b= ";
	cin >> b;

	if (a > b)
	{
		double tmp = a;
		a = b;
		b = tmp;

	}

	cout << "Podaj eps= ";
	cin >> eps;


	// - sprawdz, czy w [a, b] jest miejsce zerowe funkcji f(x)
	// - jezeli tak, to wywolaj funkcje bisekcja
	//   i wyswietl wynik na ekranie
	if((f(a)*f(b)) < 0)
	{
		double result = bisection(a, b, eps);
		cout << "Miejsce zerowe znalezione dla x = " << result << endl;
	
	}
	else // - jezeli nie, to wypisz "bledny przedzia�"
		// - zakoncz program
	{
		cout << "Bledny przedzial" << endl;


	}
	

	system("pause");
	return 0;
}

// funkcja bisection(...)
// - policz miejsce zerowe i zwroc je do programu glownego

double bisection(double a, double b, double eps)
{
	double s;
	do
	{
		s = (a + b) / 2;
		//wylicz srodek przedzialu
		if (f(s) == 0)
		{
			return s;
		}
		
	if	(f(a)*f(s) < 0)
	{
		b = s;
	}
	else
	{
		a = s;
	}
	

	} while (b - a > eps);
	
	return s;

	}

double f(double x)
{

	return x - 5;

}
